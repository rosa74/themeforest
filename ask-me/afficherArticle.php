<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<head>

    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <title>Mon petit blog</title>
    <meta name="description" content="Ask me Responsive Questions and Answers Template">
    <meta name="author" content="vbegy">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Main Style -->
    <link rel="stylesheet" href="style.css">

    <!-- Skins -->
    <link rel="stylesheet" href="css/skins/skins.css">

    <!-- Responsive Style -->
    <link rel="stylesheet" href="css/responsive.css">

    <!-- Favicons -->
    <link rel="shortcut icon" href="images/favicon.png">

</head>

<body>
    <?php
    require_once('config.php');

    ?>
    <header id="header">
        <section class="container clearfix">
            <div class="logo"><a href="blog_1.php"><img alt="" src="images/enssop_small.png"></a></div>
            <nav class="navigation">
                <ul>
                    <li class="current_page_item"><a href="blog_1.php">Mes articles</a>
                    </li>
                    <li class="current_page_item"><a href="commentaire.php">Les commentaires</a>
                    </li>
                    <li class="current_page_item"><a href="formulaireAjout.php">Ajouter un article</a>
                    </li>
                </ul>
            </nav>
        </section><!-- End container -->
    </header><!-- End header -->

    <section class="container main-content">
			<div class="row">
				<div class="col-md-9">
					<article class="post clearfix">
						<div class="post-inner">
							<div class="post-img"><a href="insertionArticle.php"><img src="http://placehold.it/810x500/222/FFF" alt=""></a></div>
							<h2 class="post-title"><span class="post-type"><i class="icon-picture"></i></span>
                                     <?php
                                        if (isset($_POST['submit'])) {
                                            $nom = htmlspecialchars(trim(($_POST['titre'])));
                                            $article = htmlspecialchars(trim(($_POST['contenu'])));
                                        }
                                            $req = $pdo->query('SELECT id, titre, contenu, DATE_FORMAT(date_creation, \'%d/%m/%Y à %Hh%imin%ss\') AS date_creation_fr FROM article ORDER BY date_creation DESC LIMIT 0, 5');
                                            $req->execute();
                                            $results = $articles->fetchAll();
                                                foreach ($articles as $article): ?>
                                                <article>
                                                    <h2><?php echo $article['titre'] ?></h2>
                                                    <p><?php echo $article['contenu'] ?></p>
                                                </article>
                                                <?php endforeach ?>
										</p>
										<a href="afficherArticle.php" class=" post-read-more button ">Continuez à lire</a>
									</div><!-- End post-content -->
						</div><!-- End post-inner -->
					</article><!-- End article.post -->

				
                <div class="pagination">
					<a href="#" class="prev-button"><i class="icon-angle-left"></i></a>
					<span class="current">1</span>

					<a href="#" class="next-button"><i class="icon-angle-right"></i></a>
				</div><!-- End pagination -->
				</div><!-- End main -->

				</aside><!-- End sidebar -->
			</div><!-- End row -->
		</section><!-- End container -->

		</footer><!-- End footer -->
		<footer id="footer-bottom">
			<section class="container">
				<div class="copyrights f_left">Copyright 2020 Mon petit blog | <a href="#">By me</a></div>
			</section><!-- End container -->
		</footer><!-- End footer-bottom -->
		</div><!-- End wrap -->


		<!-- js -->
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="js/jquery.easing.1.3.min.js"></script>
		<script src="js/html5.js"></script>
		<script src="js/twitter/jquery.tweet.js"></script>
		<script src="js/jflickrfeed.min.js"></script>
		<script src="js/jquery.inview.min.js"></script>
		<script src="js/jquery.tipsy.js"></script>
		<script src="js/tabs.js"></script>
		<script src="js/jquery.flexslider.js"></script>
		<script src="js/jquery.prettyPhoto.js"></script>
		<script src="js/jquery.carouFredSel-6.2.1-packed.js"></script>
		<script src="js/jquery.scrollTo.js"></script>
		<script src="js/jquery.nav.js"></script>
		<script src="js/tags.js"></script>
		<script src="js/jquery.bxslider.min.js"></script>
		<script src="js/custom.js"></script>
		<!-- End js -->

</body>

</html>