<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<head>

    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <title>Mon petit blog</title>
    <meta name="description" content="Ask me Responsive Questions and Answers Template">
    <meta name="author" content="vbegy">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Main Style -->
    <link rel="stylesheet" href="style.css">

    <!-- Skins -->
    <link rel="stylesheet" href="css/skins/skins.css">

    <!-- Responsive Style -->
    <link rel="stylesheet" href="css/responsive.css">

    <!-- Favicons -->
    <link rel="shortcut icon" href="images/favicon.png">

</head>

<body>
    <?php

    // connexion à la base de données
    try {

        $host = 'localhost';
        $username   = 'rosa';
        $password   = 'Leman74.';
        $database   = 'blog';

        $pdo = new PDO("mysql:host=$host;dbname=$database;", $username, $password);
        $pdo->exec('SET CHARACTER SET utf8');
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $e) {
        echo "Erreur :" . $e->getMessage();
    }
    ?>

    <header id="header">
        <section class="container clearfix">
            <div class="logo"><a href="blog_1.php"><img alt="" src="images/enssop_small.png"></a></div>
            <nav class="navigation">
                <ul>
                    <li class="current_page_item"><a href="blog_1.php">Mes articles</a>
                    </li>
                    <li class="current_page_item"><a href="commentaire.php">Les commentaires</a>
                    </li>
                    <li class="current_page_item"><a href="formulaireAjout.php">Ajouter un article</a>
                    </li>
                </ul>
            </nav>
        </section><!-- End container -->
    </header><!-- End header -->


     <?php if (isset($_POST['submit'])) {
        $nom = htmlspecialchars(trim(($_POST['titre'])));
        $article = htmlspecialchars(trim(($_POST['contenu'])));
?>

    <?php

    // Récupération du billet
   

    $req = $pdo->prepare('SELECT id, titre, contenu, DATE_FORMAT(date_creation, \'%d/%m/%Y à %Hh%imin%ss\') AS date_creation_fr FROM article WHERE id = ?');
    $req->execute(array($_GET['article']));
    $article = $req->fetch();
    ?>

    <section class="container main-content">
        <div class="row">
            <div class="col-md-9">
                <article class="post clearfix">
                    <div class="post-inner">
                        <div class="post-img"><a href="insertionArticle.php"><img src="http://placehold.it/810x500/222/FFF" alt=""></a></div>
                        <h2 class="post-title"><span class="post-type"></span><a href="commentaire.php"></h2>
                        <?php echo htmlspecialchars($article['titre']); ?>
                        <em>le <?php echo $article['date_creation_fr']; ?></em>
                        <div class="post-content">
                            <h2 class="post-title"><span class="post-type"></span></h2>

                            <?php
                            $req->closeCursor(); // Important : on libère le curseur pour la prochaine requête

                            // Récupération des commentaires
                            $req = $pdo->prepare('SELECT auteur, commentaire, DATE_FORMAT(date_commentaire, \'%d/%m/%Y à %Hh%imin%ss\') AS date_commentaire_fr FROM commentaires WHERE id_article = ? ORDER BY date_commentaire');
                            $req->execute(array($_GET['article']));

                            while ($article = $req->fetch()) {
                            ?>
                                <p><strong><?php echo htmlspecialchars($article['auteur']); ?></strong> le <?php echo $article['date_commentaire_fr']; ?></p>
                                <p><?php echo nl2br(htmlspecialchars($article['commentaire'])); ?></p>
                            <?php
                            } // Fin de la boucle des commentaires
                            $req->closeCursor();
                        }
                            ?>
                        </div><!-- End post-content -->
                    </div><!-- End post-inner -->
                </article><!-- End article.post -->
                <div id="respond" class="comment-respond page-content clearfix">
                    <div class="boxedtitle page-title">
                        <h2>Laissez un commentaire</h2>
                    </div>
                    <form action="" method="post" id="commentform" class="comment-form">
                        <div id="respond-inputs" class="clearfix">
                            <p>
                                <label class="required" for="comment_name">Name<span>*</span></label>
                                <input name="author" type="text" value="" id="comment_name" aria-required="true">
                            </p>
                        </div>
                        <div id="respond-textarea">
                            <p>
                                <label class="required" for="comment">Comment<span>*</span></label>
                                <textarea id="comment" name="comment" aria-required="true" cols="58" rows="10"></textarea>
                            </p>
                        </div>
                        <p class="form-submit">
                            <input name="submit" type="submit" id="submit" value="Post Comment" class="button small color">
                        </p>
                    </form>
                </div>

                <div class="post-next-prev clearfix">
                    <p class="prev-post">
                        <a href="#"><i class="icon-double-angle-left"></i>&nbsp;Prev post</a>
                    </p>
                    <p class="next-post">
                        <a href="#">Next post&nbsp;<i class="icon-double-angle-right"></i></a>

                        <div class="pagination">
                            <a href="#" class="prev-button"><i class="icon-angle-left"></i></a>
                            <span class="current">1</span>
                            <a href="#">2</a>

                            <a href="#" class="next-button"><i class="icon-angle-right"></i></a>
                        </div><!-- End pagination -->
                </div><!-- End main -->

                </aside><!-- End sidebar -->
            </div><!-- End row -->
    </section><!-- End container -->

    </footer><!-- End footer -->
    <footer id="footer-bottom">
        <section class="container">
            <div class="copyrights f_left">Copyright 2020 Mon petit blog | <a href="#">By me</a></div>
        </section><!-- End container -->
    </footer><!-- End footer-bottom -->
    </div><!-- End wrap -->


    <!-- js -->
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="js/jquery.easing.1.3.min.js"></script>
    <script src="js/html5.js"></script>
    <script src="js/twitter/jquery.tweet.js"></script>
    <script src="js/jflickrfeed.min.js"></script>
    <script src="js/jquery.inview.min.js"></script>
    <script src="js/jquery.tipsy.js"></script>
    <script src="js/tabs.js"></script>
    <script src="js/jquery.flexslider.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.carouFredSel-6.2.1-packed.js"></script>
    <script src="js/jquery.scrollTo.js"></script>
    <script src="js/jquery.nav.js"></script>
    <script src="js/tags.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src="js/custom.js"></script>
    <!-- End js -->

</body>

</html>